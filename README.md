# tf-ansible-wireguard

This is a Terraform plan and Ansible playbook to provision and configure a WireGuard VPN server on DigitalOcean, from nothing to fully functional including client config files (and QR code!).

For my own use, but anyone is free to use this.

To use it with DigitalOcean infrastructure and Cloudflare DNS as I do, you must have the following environment variables exported with your api keys:
```sh
export DIGITALOCEAN_TOKEN="xxxxxxxxxxxxxxxxx"
export CLOUDFLARE_EMAIL="email@example.com"
export CLOUDFLARE_API_KEY="xxxxxxxxxxxxxxxxx"
```

#### Expected Output
Once the plan/playbook run is complete, you will have a DigitalOcean VPS running WireGuard on Ubuntu 19.10 and an appropriate DNS entry for the server in Cloudflare, with `A` and `AAAA` records pointing to the WireGuard server.

You will also find a generated QR code on your client system which you can then scan with the WireGuard mobile app. If you need a config file instead, that will be generated too.

#### Requirements
* Cloudflare DNS for your domain & API key
* DigitalOcean account & API key
* Ansible installed locally
* Terraform installed locally

That's about it! Cloudflare and hosting your WG server with a domain pointing to it is optional. But you will need to tweak the Terraform config to exclude this.
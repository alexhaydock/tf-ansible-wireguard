# Import the $TF_VAR_SSHKEY shell variable so we can use it to find our SSH key below
variable "SSHKEY" {}

# Create a new Web Droplet in the nyc2 region
resource "digitalocean_droplet" "wireguard_server" {
  image    = "ubuntu-19-10-x64"
  name     = "wireguard"
  region   = "lon1"
  size     = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.add_ssh_key.id]
  ipv6     = true

  # Use remote-exec to run a pointless command on the remote server because remote-exec will wait for the
  # server instance to deploy properly, whereas local-exec wouldn't. If we just try to run our Ansible
  # playbook immediately then Vultr probably won't have finished deploying the server before it tries to run.
  provisioner "remote-exec" {
    inline = ["echo Hello_World"]

    connection {
      host        = digitalocean_droplet.wireguard_server.ipv4_address
      type        = "ssh"
      user        = "root"
      private_key = file(var.SSHKEY)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${digitalocean_droplet.wireguard_server.ipv4_address},' --private-key '${var.SSHKEY}' wireguard.yml" 
  }
}
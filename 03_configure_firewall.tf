// This var holds our public IP so we can configure the security groups correctly.
// We export this from the command line when running the Terraform plan.
variable "PUBIP" {}

resource "digitalocean_firewall" "firewall_config" {
  name = "tf-wireguard-server-firewall"

  droplet_ids = [digitalocean_droplet.wireguard_server.id]

  // Allow SSH in just from current public IP running the Terraform plan
  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = [var.PUBIP]
  }

  // Allow HTTPS / DoH in from anywhere
  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  // Allow DoT in from anywhere
  inbound_rule {
    protocol         = "tcp"
    port_range       = "853"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  // Allow WireGuard TCP in from anywhere
  inbound_rule {
    protocol         = "tcp"
    port_range       = "51820"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  // Allow WireGuard UDP in from anywhere
  inbound_rule {
    protocol         = "udp"
    port_range       = "51820"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  // Respond to pings
  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  // Allow outbound TCP traffic anywhere
  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  // Allow outbound UDP traffic anywhere
  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  // Allow outbound pings
  outbound_rule {
    protocol              = "icmp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}
// find this in the dashboard page for the domain in Cloudflare
variable "cloudflare_zone_id" {
  default = "bcf2464ea80f6c630b5eb08cac4feba8"
}

// create A record for server
resource "cloudflare_record" "A" {
  zone_id  = var.cloudflare_zone_id
  name     = "wg"
  type     = "A"
  ttl      = 3600
  proxied  = false
  value    = digitalocean_droplet.wireguard_server.ipv4_address
}

// create AAAA record for server
resource "cloudflare_record" "AAAA" {
  zone_id  = var.cloudflare_zone_id
  name     = "wg"
  type     = "AAAA"
  ttl      = 3600
  proxied  = false
  value    = digitalocean_droplet.wireguard_server.ipv6_address
}
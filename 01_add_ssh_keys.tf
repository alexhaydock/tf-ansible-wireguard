# Import the $TF_VAR_SSHPUBKEY shell variable so we can use it to find our SSH key below
variable "SSHPUBKEY" {}

resource "digitalocean_ssh_key" "add_ssh_key" {
  name       = "terraform-ssh-key"
  public_key = file(var.SSHPUBKEY)
}